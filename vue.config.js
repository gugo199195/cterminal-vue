const path = require("path");

module.exports = {
  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'ru',
      localeDir: 'locales',
      enableInSFC: true
    },
    "style-resources-loader": {
      preProcessor: "scss",
      patterns: [
          path.resolve(__dirname, "./src/assets/scss/globals/variables/colors.scss"),
          path.resolve(__dirname, "./src/assets/scss/globals/mixins/inline-svg.scss"),
          path.resolve(__dirname, "./src/assets/scss/globals/mixins/typography.scss"),
          path.resolve(__dirname, "./src/assets/scss/globals/utilities.scss"),
          path.resolve(__dirname, "./src/assets/scss/main.scss"),
      ]
    },
  },
};
