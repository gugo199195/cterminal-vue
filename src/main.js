import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import { store } from "./stores/store";
import router from './router'
import i18n from './i18n'
import FloatFilterDirective from '@/assets/js/directives/floatFilter.js';

import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios);

import VueCookie from 'vue-cookie';
Vue.use(VueCookie);

import VueDragResize from './plugins/vue-drag-resize/src/index'
Vue.component('vue-drag-resize', VueDragResize)

import lazyLoadList from 'vue-lazy-load-list';
Vue.use(lazyLoadList);

import VueCookies from 'vue-cookies'
Vue.use(VueCookies)
import 'vuetify/dist/vuetify.min.css'

import Default from "./layouts/default";
import Base from "./layouts/_base";
import Admin from "./layouts/admin";

Vue.component("default-layout", Default);
Vue.component("base-layout", Base);
Vue.component("admin-layout", Admin);

//Directives
Vue.directive("float-filter", FloatFilterDirective);



Vue.config.productionTip = false

new Vue({
  router,
  i18n,
  store,
  render: h => h(App)
}).$mount('#app')
