
export default {
  computed: {
    getDarkTheme(){
      return this.$store.getters['getDarkTheme']
    }
  },
};
