import Vue from "vue";
import Vuex from "vuex";

import state from "./state";
import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";

import Home from "./modules/home/store";
import ActionManager from "./modules/actionManager/store";
import WindowManager from "./modules/windowManager/store";
import TradingManager from "./modules/tradingManager/store";
import BalancesManager from "./modules/balancesManager/store";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  modules: {
    home: Home,
    actionManager: ActionManager,
    windowManager: WindowManager,
    tradingManager: TradingManager,
    balancesManager: BalancesManager,
  }
});
