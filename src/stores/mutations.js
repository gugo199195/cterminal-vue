export default {
    SET_LANG(state, locale) {
        if (state.locales[locale]) {
            state.locale = locale
        }
    }
}