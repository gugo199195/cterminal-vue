export default {
    locales: {
        en:  {
            name: 'en',
            text: 'English',
        },
        ru: {
            name: 'ru',
            text: 'Русский',
        }
    },
    locale: 'en'
}