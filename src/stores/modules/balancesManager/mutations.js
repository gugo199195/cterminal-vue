import {SET_SELECTED_ACCOUNTS} from "./api/mutation-types";

export default {
  [SET_SELECTED_ACCOUNTS](state, payload) {
    state.selectedAccounts = payload.selectedAccounts
  },
};
