import axios from 'axios'
import * as types from "./api/mutation-types";

export default {
  setSelectedAccounts({commit}, {selectedAccounts}) {
    commit(types.SET_SELECTED_ACCOUNTS, {selectedAccounts: selectedAccounts});
  },
}
