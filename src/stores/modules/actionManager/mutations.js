export default {
  toggleColorTheme(state) {
    state.darkTheme = !state.darkTheme;
  },
  setColorTheme(state, isDarkTheme) {
    state.darkTheme = isDarkTheme;
  },
  setSelectedArticleLink(state, selectedArticleLink) {
    state.selectedArticleLink = selectedArticleLink;
  }
};
