import axios from 'axios'
import * as types from "./types";

export default {
  [types.A_CHECK_TEST]: ({ commit }, payload) => {
    commit('checkTest', payload);
  }
}
