export default {
  historyPosition: 0,
  windowsHistory: [],
  windowsData: [],
  layoutInitialized: false,
  maximizedLayout: {
    left: 0,
    top: 0,
    width: 0,
    height: 0,
  },
  fixedContainer: {
    y: 1160,
  },
  container: {
    x: 0,
    y: 0,
    offsetTop: 0,
    offsetLeft: 0,
  },
  multipleSettings: {
    graph: {
      maxCount: 6,
      currentCount: 0,
      templateType: 'grid',
    }
  },
  windows: [
    {
      name: 'pairSelector',
      component: 'pair-selector',
      title: 'title.trading_pair_selector',
      icon: 'repeat',
      defaultLayout: {
        position: {
          x: 1,
          y: 1
        },
        size: {
          x: 1,
          y: 3.5
        },
        minw: 300,
        minh: 483,
      },
    },
    {
      name: 'order',
      component: 'order',
      title: 'title.trading_order',
      icon: 'get_app',
      defaultLayout: {
        position: {
          x: 1,
          y: 4.5
        },
        size: {
          x: 1,
          y: 2.5
        },
        minw: 300,
        minh: 451,
      },
    },
    {
      name: 'graph',
      component: 'graph',
      title: 'title.trading_graph',
      dataLockable: true,
      icon: 'timeline',
      defaultLayout: {
        position: {
          x: 2,
          y: 1
        },
        size: {
          x: 2,
          y: 3.5
        },
        minw: 540,
        minh: 415,
      },
    },
    {
      name: 'openedOrders',
      component: 'opened-orders',
      title: 'title.trading_opened_orders',
      icon: 'view_list',
      defaultLayout: {
        position: {
          x: 2,
          y: 4.5
        },
        size: {
          x: 2,
          y: 2.5
        },
        minw: 540,
        minh: 270,
      },
    },
    {
      name: 'orderBook',
      component: 'order-book',
      title: 'title.trading_order_book',
      icon: 'book',
      defaultLayout: {
        position: {
          x: 4,
          y: 1
        },
        size: {
          x: 1,
          y: 2.75
        },
        minw: 300,
        minh: 375,
      },
    },
    {
      name: 'history',
      component: 'history',
      title: 'title.trading_history',
      icon: 'history',
      defaultLayout: {
        position: {
          x: 4,
          y: 3.75
        },
        size: {
          x: 1,
          y: 1.625
        },
        minw: 300,
        minh: 325,
      },
    },
    {
      name: 'news',
      component: 'news',
      title: '',
      icon: 'new_releases',
      defaultLayout: {
        position: {
          x: 4,
          y: 5.375
        },
        size: {
          x: 1,
          y: 1.625
        },
        minw: 300,
        minh: 350,
      },
    }
  ],
  topWindowIndex: 1,
  maxHeightColumn: 1080
}
