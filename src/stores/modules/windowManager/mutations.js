import {
  ACTIVE_CLICK_ARM,
  ADD_WINDOW,
  CHANGE_DATA_LOCK,
  CHANGE_LAYOUT,
  CHANGE_ZINDEX, DECREASE_WINDOW_COUNT,
  DISABLE_ACTIVE,
  DISABLE_MAXIMIZED,
  ENABLE_ACTIVE,
  ENABLE_MAXIMIZED,
  HIDE_WINDOW, INCREASE_WINDOW_COUNT,
  INIT_LAYOUT, M_SET_WINDOW_ARM, READ_HISTORY,
  REMOVE_WINDOW,
  SET_ADDITIONAL_TITLE, SET_CONTAINER, SET_MULTIPLE_TEMPLATE_TYPE,
  SET_TOP_WINDOW_INDEX,
  SET_WINDOWS_DATA,
  SHOW_WINDOW, WRITE_HISTORY,
  CLEAR_HSTORY
} from "./api/mutation-types";

const maxHistoryLength = 20;

export default {
  [CLEAR_HSTORY](state) {
    state.windowsHistory = []
  },
  [ENABLE_ACTIVE](state, id) {
    for(let i=0; i < state.windowsData.length; i++) {
      state.windowsData[i].isActive = false;
    }
    state.windowsData[id.id].isActive = true;
  },
  [DISABLE_ACTIVE](state, id) {
    state.windowsData[id].isActive = false;
  },
  [ENABLE_MAXIMIZED](state, id) {
    state.windowsData[id].maximized = true;
  },
  [DISABLE_MAXIMIZED](state, id) {
    state.windowsData[id].maximized = false;
  },
  [SHOW_WINDOW](state, id) {
    state.windowsData[id].show = true;
  },
  [HIDE_WINDOW](state, id) {
    state.windowsData[id].show = false;
  },
  [CHANGE_DATA_LOCK](state, id) {
    state.windowsData[id].dataLocked = !state.windowsData[id].dataLocked;
  },
  [CHANGE_ZINDEX](state, payload) {
    state.windowsData[payload.id].zIndex = payload.zIndex < 1 ? 1 : payload.zIndex;
  },
  [CHANGE_LAYOUT](state, payload) {
    state.windowsData[payload.id].windowLayout = payload.layout;
  },
  [INIT_LAYOUT](state) {
    state.layoutInitialized = true;
  },
  [SET_ADDITIONAL_TITLE](state, payload) {
    state.windowsData[payload.id].additional_title = payload.additionalTitle;
  },
  [SET_WINDOWS_DATA](state, windowsData) {
    state.windowsData = windowsData;
  },
  [ADD_WINDOW](state, window) {
    state.windowsData.push(window);
  },
  [REMOVE_WINDOW](state, id) {
    state.windowsData.splice(id, 1);
  },
  [SET_TOP_WINDOW_INDEX](state, index) {
    state.topWindowIndex = index;
  },
  [SET_CONTAINER](state, container) {
    state.container = {...container};
    // state.container.x = container.x;
    // state.container.y = container.y;
    // state.container.offsetLeft = container.offsetLeft;
    // state.container.offsetTop = container.offsetTop;
  },
  [INCREASE_WINDOW_COUNT](state, windowName) {
    state.multipleSettings[windowName].currentCount = state.multipleSettings[windowName].currentCount + 1;
  },
  [DECREASE_WINDOW_COUNT](state, windowName) {
    state.multipleSettings[windowName].currentCount = state.multipleSettings[windowName].currentCount - 1;
  },
  [SET_MULTIPLE_TEMPLATE_TYPE](state, payload) {
    state.multipleSettings[payload.windowName].templateType = payload.templateType;
  },
  [WRITE_HISTORY](state, actionData) {

    for (let i = 0; i < state.historyPosition; i++) {
      state.windowsHistory.shift();
    }
    state.historyPosition = 0;

    if (
      state.windowsHistory[0] &&
      state.windowsHistory[0].type === actionData.type &&
      state.windowsHistory[0].windowId === actionData.id
    ) {
      return
    }

    state.windowsHistory.unshift({
        windowsData: JSON.stringify(state.windowsData),
        type: actionData.type,
        windowId: actionData.id
    });

    if (state.windowsHistory.length > maxHistoryLength) {
      state.windowsHistory.pop();
    }
  },
  [READ_HISTORY](state, id) {
    state.windowsData = JSON.parse(state.windowsHistory[id].windowsData);
    state.historyPosition = id;
  },


  [ACTIVE_CLICK_ARM](state, data) {
    state.windowsData[data.id].zIndex = state.topWindowIndex < 1 ? 1 : state.topWindowIndex;
    state.topWindowIndex = state.topWindowIndex + 1;
    for(let i=0; i < state.windowsData.length; i++) {
      if(state.windowsData[i].isActive === true) {
        state.windowsData[i].isActive = false;
        return
      }
    }
    state.windowsData[data.id].isActive = true;
  },


  [M_SET_WINDOW_ARM](state, data) {
    state.windowsData[data.id].zIndex = state.topWindowIndex < 1 ? 1 : state.topWindowIndex;
    state.topWindowIndex = state.topWindowIndex + 1;
    for(let i=0; i < state.windowsData.length; i++) {
      if(state.windowsData[i].isActive === true) {
        state.windowsData[i].isActive = false;
        return
      }
    }
    state.windowsData[data.id].isActive = true;
    state.windowsData[data.id].windowLayout = data.layout;
    for (let i = 0; i < state.historyPosition; i++) {
      state.windowsHistory.shift();
    }
    state.historyPosition = 0;
    if (
      state.windowsHistory[0] &&
      state.windowsHistory[0].type === data.type &&
      state.windowsHistory[0].windowId === data.id
    ) {
      return
    }
    state.windowsHistory.push({
      windowsData: JSON.stringify(state.windowsData),
      type: data.type,
      windowId: data.id
    });
    if (state.windowsHistory.length > maxHistoryLength) {
      state.windowsHistory.pop();
    }
  }
};
