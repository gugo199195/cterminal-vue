import axios from 'axios'
import * as types from "./api/mutation-types";

const gridParameters = {
  space: 15,
  x: 4,
  y: 6,
  cascadeShift: 35,
};
const windowTemplate = {
  additional_title: '',
  zIndex: 1,
  isActive: false,
  show: true,
  maximized: false,
  dataLockable: false,
  dataLocked: false,
  windowLayout: {
    left: null,
    top: null,
    width: null,
    height: null,
  },
  sticks: ['br'],
};

export default {
  setAdditionalTitle({commit}, {windowId, additionalTitle}) {
    commit(types.SET_ADDITIONAL_TITLE, {id: windowId, additionalTitle: additionalTitle});
  },
  setWindow({commit, state}, {windowId, layout, type}) {
    if (!state.windowsData[windowId].maximized) {
      commit(types.M_SET_WINDOW_ARM, {id: windowId, layout: layout, type: type});
      commit(types.CHANGE_LAYOUT, {id: windowId, layout: layout});
      commit(types.WRITE_HISTORY, {type: type, id: windowId});
    }
  },
  activateWindow({dispatch,commit}, payload) {
    commit(types.ACTIVE_CLICK_ARM, {id: payload.windowId,});
    dispatch('changeZToTop', {windowId:payload.windowId});
    commit(types.ENABLE_ACTIVE, {id:payload.windowId});
  },
  showWindow({dispatch, commit}, {windowId}) {
    commit(types.SHOW_WINDOW, windowId);
    dispatch('activateWindow', {windowId});
    commit(types.WRITE_HISTORY, {type: 'window_show', id: windowId});
  },
  hideWindow({commit}, {windowId}) {
    commit(types.HIDE_WINDOW, windowId);
    commit(types.WRITE_HISTORY, {type: 'window_hide', id: windowId});
  },
  toggleWindow({dispatch, commit, state}, {windowId}) {
    if (state.windowsData[windowId].show) {
      commit(types.HIDE_WINDOW, windowId);
      commit(types.WRITE_HISTORY, {type: 'window_hide', id: windowId});
    } else {
      commit(types.SHOW_WINDOW, windowId);
      dispatch('activateWindow', {windowId});
      commit(types.WRITE_HISTORY, {type: 'window_show', id: windowId});
    }
  },
  changeZToTop({commit, state}, {windowId}) {
    commit(types.CHANGE_ZINDEX, {id: windowId, zIndex: state.topWindowIndex});
    commit(types.SET_TOP_WINDOW_INDEX, state.topWindowIndex + 1);
  },
  resetWindows({dispatch, commit, state}) {
    state.windows.forEach((window) => {
      dispatch('resetWindowByName', {windowName: window.name})
    });
    commit(types.WRITE_HISTORY, {type: 'windows_reset', id: null});
  },
  resetContainer({commit}, container) {
    commit(types.SET_CONTAINER, container);
  },
  initLayout({dispatch, commit, state}, container) {
    if (state.layoutInitialized) {
      return
    }

    let windowsArray = [];
    let zIndex = state.topWindowIndex;
    state.windows.forEach((window)=>{
      let windowTmp = Object.assign({}, windowTemplate);
      windowTmp = Object.assign(windowTmp, window);
      windowTmp.zIndex = zIndex;

      zIndex++;
      windowsArray.push(windowTmp);
    });
    windowsArray[0].windowLayout.width = 444

    commit(types.SET_WINDOWS_DATA, windowsArray);
    commit(types.SET_TOP_WINDOW_INDEX, zIndex);

    dispatch('resetContainer', container);
    dispatch('resetWindows');
    commit(types.INIT_LAYOUT);
  },
  addWindow({commit, state, dispatch}, {windowName}) {
    let window = state.windows.filter((window)=>{
      return window.name === windowName;
    })[0];
    let windowTmp = Object.assign({}, windowTemplate);
    windowTmp = Object.assign(windowTmp, window);
    windowTmp.zIndex = state.topWindowIndex + 1;
    commit(types.SET_TOP_WINDOW_INDEX, state.topWindowIndex + 1);
    commit(types.ADD_WINDOW, windowTmp);
    commit(types.INCREASE_WINDOW_COUNT, windowName);

    dispatch('resetWindow', {
      windowId: state.windowsData.length - 1,
      putTop: false
    });
  },
  removeWindow({commit, state}, {windowId}) {
    commit(types.DECREASE_WINDOW_COUNT, state.windowsData[windowId].name);
    commit(types.REMOVE_WINDOW, windowId);
  },
  toggleMaximized({commit, state}, {windowId}) {
    if (state.windowsData[windowId].maximized) {
      commit(types.DISABLE_MAXIMIZED, windowId);
      commit(types.WRITE_HISTORY, {type: 'window_minimized', id: windowId});
    } else {
      commit(types.ENABLE_MAXIMIZED, windowId);
      commit(types.WRITE_HISTORY, {type: 'window_maximized', id: windowId});
    }
  },
  hideWindows({commit, state}) {
    state.windowsData.forEach((windowData, windowId) => {
      commit(types.HIDE_WINDOW, windowId);
    });
    commit(types.WRITE_HISTORY, {type: 'windows_hide', id: null});
  },
  historyUndo({commit, state}) {
    let newHistoryPosition = state.historyPosition + 1;
    if (newHistoryPosition < state.windowsHistory.length) {
      commit(types.READ_HISTORY, newHistoryPosition);
    }
  },
  historyRedo({commit, state}) {
    if (state.historyPosition > 0) {
      commit(types.READ_HISTORY, state.historyPosition - 1);
    }
  },
  setTemplateType({commit}, {windowName, templateType}) {
    commit(types.SET_MULTIPLE_TEMPLATE_TYPE, {windowName: windowName, templateType: templateType});
  },
  changeDataLock({commit}, {windowId}) {
    commit(types.CHANGE_DATA_LOCK, windowId);
  },
  resetWindow({commit, state}, {windowId, putTop = false}) {
    let windowData = state.windowsData[windowId];
    let left = 0;
    let top = 0;
    let blockSizeX = (state.container.x - (gridParameters.space * (gridParameters.x + 1) )) / gridParameters.x;
    let blockSizeY = (state.container.y - (gridParameters.space * (gridParameters.y + 1) )) / gridParameters.y;

    commit(types.SHOW_WINDOW, windowId);
    commit(types.DISABLE_ACTIVE, windowId);
    commit(types.DISABLE_MAXIMIZED, windowId);

    if (putTop) {
      left = state.container.offsetLeft + gridParameters.space;
      top = state.container.offsetTop + gridParameters.space;
    } else {
      left = state.container.offsetLeft + Math.floor(gridParameters.space * windowData.defaultLayout.position.x + blockSizeX * (windowData.defaultLayout.position.x - 1));
      top = state.container.offsetTop +  Math.floor(gridParameters.space * windowData.defaultLayout.position.y + blockSizeY * (windowData.defaultLayout.position.y - 1));
    }
    let layout = {
      left: left,
      top: top,
      width: Math.floor(blockSizeX * windowData.defaultLayout.size.x + gridParameters.space * (windowData.defaultLayout.size.x - 1)),
      height: Math.floor(blockSizeY * windowData.defaultLayout.size.y + gridParameters.space * (windowData.defaultLayout.size.y - 1)),
    };

    commit(types.CHANGE_LAYOUT, {id: windowId, layout: layout});
    if (putTop) {
      commit(types.WRITE_HISTORY, {type: 'window_reset', id: windowId});
    }
  },
  resetWindowHistory({commit, state}) {
    commit(types.CLEAR_HSTORY);
  },
  resetWindowByName({commit, state}, {windowName}) {
    let blockSizeX = (state.container.x - (gridParameters.space * (gridParameters.x + 1) )) / gridParameters.x;
    let blockSizeY = (state.container.y - (gridParameters.space * (gridParameters.y + 1) )) / gridParameters.y;
    let window = state.windows.filter((element) => {
      return element.name === windowName
    })[0];

    let layout = {
      left: state.container.offsetLeft + Math.floor(gridParameters.space * window.defaultLayout.position.x + blockSizeX * (window.defaultLayout.position.x - 1)),
      top: state.container.offsetTop +  Math.floor(gridParameters.space * window.defaultLayout.position.y + blockSizeY * (window.defaultLayout.position.y - 1)),
      width: Math.floor(blockSizeX * window.defaultLayout.size.x + gridParameters.space * (window.defaultLayout.size.x - 1)),
      height: Math.floor(blockSizeY * window.defaultLayout.size.y + gridParameters.space * (window.defaultLayout.size.y - 1)),
    };
    if (typeof state.multipleSettings[windowName] === 'undefined') {
      state.windowsData.forEach((windowData, windowId) => {
        if (windowData.name === windowName) {
          if(windowData.component == "opened-orders" && layout.left < 390) {
            layout.left = 390;
            layout.width = 540;
          }
          if(windowData.component == "opened-orders" && layout.top < 445) {
            layout.top = 439;
            layout.height = 270;
          }

          switch (windowData.component) {
              case "pair-selector":
                if(layout.width < 300) {
                  layout.width = 300;
                }
                if(layout.height < 483) {
                  layout.height = 483;
                }
                layout.top = 9;
                break;

              case "order":
                if(layout.width < 300) {
                  layout.width = 300;
                }
                if(layout.height < 451) {
                  layout.height =  state.fixedContainer.y - state.windowsData[windowId - 1].windowLayout.height;
                  layout.top = state.windowsData[windowId - 1].windowLayout.height + 30;
                }
                break;

              case "graph":
                if(layout.width <= 540) {
                  layout.left = 390;
                  layout.width = 540;
                }
                if(layout.height < 415) {
                  layout.height = 415;
                }
                layout.top = 9;
                break;

              case "opened-orders":
                if(layout.width <= 540) {
                  layout.left = 390;
                  layout.width = 540;
                }
                if(layout.height < 270) {
                  layout.height = 270;
                  layout.top = 439;
                }
                break;

              case "order-book":
                if(layout.width < 300) {
                  layout.left = 945;
                  layout.width = 300;
                }
                if(layout.height < 375) {
                  layout.height = 375;
                }
                layout.top = 9;
                break;

              case "history":
                if(layout.width < 300) {
                  layout.left = 945;
                  layout.width = 300;
                }
                if(layout.height < 325) {
                  layout.height = 325;
                  layout.top = state.windowsData[windowId - 1].windowLayout.height + 30;
                }
                break;

              case "news":
                if(layout.width < 300) {
                  layout.left = 945;
                  layout.width = 300;
                }
                if(layout.height < 350) {
                  layout.height = 350;
                  layout.top = state.windowsData[windowId - 1].windowLayout.height + state.windowsData[windowId - 2].windowLayout.height + 45;
                }
                break;

            }
          commit(types.SHOW_WINDOW, windowId);
          commit(types.DISABLE_ACTIVE, windowId);
          commit(types.DISABLE_MAXIMIZED, windowId);
          commit(types.CHANGE_LAYOUT, {id: windowId, layout: layout});
        }
      });
    } else {
      let windowsData = [];
      state.windowsData.forEach((item, index) => {
        if (item.name === windowName) {
          windowsData.push({
            data: item,
            windowId: index,
          });
        }
      });
      windowsData.sort((item1, item2) => {
        return item1.data.zIndex > item2.data.zIndex;
      });

      if (state.multipleSettings[windowName].templateType === 'grid') {
        let columns = Math.ceil(Math.sqrt(windowsData.length));
        let rows = Math.ceil(windowsData.length / columns);
        let localBlockSizeX = (layout.width - (gridParameters.space * (columns - 1))) / columns;
        let localBlockSizeY = (layout.height - (gridParameters.space * (rows - 1))) / rows;
        layout.top = 9;
        for (let c = 0; c < columns; c++) {
          for (let r = 0; r < rows; r++) {
            if (c * rows + r > windowsData.length - 1) {
              break;
            }
            let localLayout = {
              left: Math.floor(gridParameters.space * c + localBlockSizeX * c + layout.left),
              top: Math.floor(gridParameters.space * r + localBlockSizeY * r + layout.top),
              width: localBlockSizeX,
              height: localBlockSizeY,
            };
            let graphsWindow = windowsData.filter(val => {
              return val.data.component == 'graph'
            });
            windowsData.forEach(item => {
              if(item.data.component == "graph" && layout.left < 390) {
                  localLayout.left = 390;
                localLayout.width = 540;

              } else if(item.data.component == "opened-orders" && layout.left < 390) {
                localLayout.width = 540;
                localLayout.left = 390;
              }
              if(item.data.component == "graph" && layout.height < 415 && graphsWindow.length < 3) {
                localLayout.height = 415;
              }
            });
            commit(types.SHOW_WINDOW, windowsData[c * rows + r].windowId);
            commit(types.DISABLE_ACTIVE, windowsData[c * rows + r].windowId);
            commit(types.DISABLE_MAXIMIZED, windowsData[c * rows + r].windowId);
            commit(types.CHANGE_LAYOUT, {id: windowsData[c * rows + r].windowId, layout: localLayout});
          }
        }
      } else if (state.multipleSettings[windowName].templateType === 'cascade') {
        let localBlockSizeX = layout.width - (gridParameters.cascadeShift * (windowsData.length - 1));
        let localBlockSizeY = layout.height - (gridParameters.cascadeShift * (windowsData.length - 1));
        layout.top = 9;
        for (let i = 0; i < windowsData.length; i++) {
          let localLayout = {
            left: layout.left + i * gridParameters.cascadeShift,
            top: layout.top + i * gridParameters.cascadeShift,
            width: localBlockSizeX,
            height: localBlockSizeY,
          };
          if(windowsData[i].data.component == "graph" && layout.left < 390) {
            localLayout.left = 390;
            localLayout.width = 540;
          }
          if(windowsData[i].data.component == "graph" && layout.height < 415) {
            localLayout.height = 415;

          }
          commit(types.SHOW_WINDOW, windowsData[i].windowId);
          commit(types.DISABLE_ACTIVE, windowsData[i].windowId);
          commit(types.DISABLE_MAXIMIZED, windowsData[i].windowId);
          commit(types.CHANGE_LAYOUT, {id: windowsData[i].windowId, layout: localLayout});
        }
      }
    }
  },
  changeMultipleType({commit, state}, {windowName}) {
    let blockSizeX = (state.container.x - (gridParameters.space * (gridParameters.x + 1) )) / gridParameters.x;
    let blockSizeY = (state.container.y - (gridParameters.space * (gridParameters.y + 1) )) / gridParameters.y;
    let window = state.windows.filter((element) => {
      return element.name === windowName
    })[0];

    let layout = {
      left: state.container.offsetLeft + Math.floor(gridParameters.space * window.defaultLayout.position.x + blockSizeX * (window.defaultLayout.position.x - 1)),
      top: state.container.offsetTop +  Math.floor(gridParameters.space * window.defaultLayout.position.y + blockSizeY * (window.defaultLayout.position.y - 1)),
      width: Math.floor(blockSizeX * window.defaultLayout.size.x + gridParameters.space * (window.defaultLayout.size.x - 1)),
      height: Math.floor(blockSizeY * window.defaultLayout.size.y + gridParameters.space * (window.defaultLayout.size.y - 1)),
    };
    let windowsData = [];
    state.windowsData.forEach((item, index) => {
      if (item.name === windowName) {
        windowsData.push({
          data: item,
          windowId: index,
        });
      }
    });
    windowsData.sort((item1, item2) => {
      return item1.data.zIndex > item2.data.zIndex;
    });

    if (state.multipleSettings[windowName].templateType === 'grid') {
      let columns = Math.ceil(Math.sqrt(windowsData.length));
      let rows = Math.ceil(windowsData.length / columns);
      let localBlockSizeX = (layout.width - (gridParameters.space * (columns - 1))) / columns;
      let localBlockSizeY = ((layout.height - (gridParameters.space * (rows - 1))) / rows);

      for (let c = 0; c < columns; c++) {
        for (let r = 0; r < rows; r++) {
          if (c * rows + r > windowsData.length - 1) {
            break;
          }
          let graphsWindow = windowsData.filter(val => {
            return val.data.component == 'graph'
          });
          let localLayout = {
            left: Math.floor(gridParameters.space * c + localBlockSizeX * c + layout.left),
            top: Math.floor(gridParameters.space * r + localBlockSizeY * r + layout.top),
            width: localBlockSizeX,
            height: localBlockSizeY,
          };
          windowsData.forEach(item => {
            if(item.data.component == "graph" && layout.left < 390) {
              console.log(1)
              localLayout.left = 390;
              localLayout.width = 540;
            }
            if(item.data.component == "graph" && layout.height < 415 && graphsWindow.length < 3) {
              console.log(3)
              localLayout.height = 415;
            } else if(item.data.component == "graph" && layout.height < 415 && graphsWindow.length > 3) {
              console.log(55)
            }
          });
          commit(types.SHOW_WINDOW, windowsData[c * rows + r].windowId);
          commit(types.DISABLE_ACTIVE, windowsData[c * rows + r].windowId);
          commit(types.DISABLE_MAXIMIZED, windowsData[c * rows + r].windowId);
          commit(types.CHANGE_LAYOUT, {id: windowsData[c * rows + r].windowId, layout: localLayout});
        }
      }
    } else if (state.multipleSettings[windowName].templateType === 'cascade') {
      let localBlockSizeX = layout.width - (gridParameters.cascadeShift * (windowsData.length - 1));
      let localBlockSizeY = layout.height - (gridParameters.cascadeShift * (windowsData.length - 1));
      for (let i = 0; i < windowsData.length; i++) {
        let localLayout = {
          left: layout.left + i * gridParameters.cascadeShift,
          top: layout.top + i * gridParameters.cascadeShift,
          width: localBlockSizeX,
          height: localBlockSizeY,
        };
        if(windowsData[i].data.component == "graph" && layout.left < 390) {
          localLayout.left = 390;
          localLayout.width = 540;
        }
        if(windowsData[i].data.component == "graph" && layout.height < 415) {
          localLayout.height = 415 - (windowsData.length*30);

        }
        commit(types.SHOW_WINDOW, windowsData[i].windowId);
        commit(types.DISABLE_ACTIVE, windowsData[i].windowId);
        commit(types.DISABLE_MAXIMIZED, windowsData[i].windowId);
        commit(types.CHANGE_LAYOUT, {id: windowsData[i].windowId, layout: localLayout});
      }
    }
  }
}
