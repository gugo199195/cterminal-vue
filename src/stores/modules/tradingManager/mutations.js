  import {
    SET_ACCOUNTS,
    SET_BALANCE_ACCOUNT,
    SET_BALANCES,
    SET_BALANCES_HISTORY,
    SET_BASE_CURRENCY,
    SET_EXCHANGE_CODE,
    SET_EXCHANGE_CURRENCY_CODES,
    SET_EXCHANGES,
    SET_FAVORITE_EXCHANGES,
    SET_FAVORITE_MARKETS,
    SET_FAVORITE_TICKERS,
    SET_MARKET,
    SET_ORDER_ACCOUNT,
    SET_ORDER_AMOUNT,
    SET_ORDER_BOOK,
    SET_ORDER_PRICE,
    SET_ORDER_TYPE,
    SET_ORDERS,
    SET_QUOTE_CURRENCY,
    SET_STATISTIC_CURRENCIES, SET_USER_EXCHANGES,
    UPDATE_ORDER_BOOK, UPDATE_TICKERS
  } from "./api/mutation-types";

export default {
  [SET_ACCOUNTS](state, payload) {
    state.accounts = payload.accounts;
    if (state.balanceAccount && typeof(state.accounts[state.balanceAccount.id]) === 'undefined') {
      state.balanceAccount = null;
    }
  },
  [SET_BALANCE_ACCOUNT](state, payload) {
    state.balanceAccount = payload.balanceAccount;
  },
  [SET_BALANCES](state, payload) {
    state.balances = payload.balances;
  },
  [SET_BALANCES_HISTORY](state, payload) {
    state.balancesHistory = payload.balancesHistory;
  },
  [SET_BASE_CURRENCY](state, payload) {
    state.baseCurrency = payload.baseCurrency;
  },
  [SET_EXCHANGE_CODE](state, payload) {
    state.baseCurrency = null;
    state.quoteCurrency = null;
    state.exchangeCode = payload.exchangeCode;
  },
  [SET_EXCHANGE_CURRENCY_CODES](state, payload) {
    state.exchangeCurrencyCodes = payload.exchangeCurrencyCodes;
  },
  [SET_EXCHANGES](state, payload) {
    state.exchanges = payload.exchanges
  },
  [SET_FAVORITE_EXCHANGES](state, payload) {
    state.favoriteExchanges = payload.favoriteExchanges;
  },
  [SET_FAVORITE_MARKETS](state, payload) {
    state.favoriteMarkets = payload.favoriteMarkets;
  },
  [SET_FAVORITE_TICKERS](state, payload) {
    state.favoriteTickers = payload.favoriteTickers;
  },
  [SET_MARKET](state, payload) {
    state.market = payload.market;
  },
  [SET_ORDER_ACCOUNT](state, payload) {
    state.orderAccount = payload.orderAccount;
  },
  [SET_ORDER_AMOUNT](state, payload) {
    state.orderAmount = payload.orderAmount;
  },
  [SET_ORDER_BOOK](state, payload) {
    state.orderBook = payload.orderBook;
  },
  [UPDATE_ORDER_BOOK](state, payload) {
    let prices = {
      asks: state.orderBook.asks,
      bids: state.orderBook.bids,
    }

    Object.keys(payload.diff).forEach((priceType) => {
      payload.diff[priceType].forEach((change) => {
        if (parseFloat(change[1]) === 0) {
          prices[priceType] = prices[priceType].filter((item) => {
            return item[0] !== change[0];
          });
        } else {
          if (prices[priceType].filter((item) => {
            return item[0] === change[0];
          }).length === 0) {

            let k = (priceType === 'asks') ? -1 : 1;
            prices[priceType].push(change);
            prices[priceType].sort((item1, item2) => {
              return parseFloat(item1[0]) > parseFloat(item2[0]) ? -k : k;
            });
          } else {

            prices[priceType] = prices[priceType].map((item) => {
              if (item[0] === change[0]) {
                item[1] = change[1];
              }

              return item;
            });
          }

          if (prices[priceType].length > 100) {
            prices[priceType].length = 100;
          }
        }
      })
    });

    state.orderBook = prices;
  },
  [SET_ORDER_PRICE](state, payload) {
    state.orderPrice = payload.orderPrice;

    if (payload.orderPrice !== null) {
      state.orderType = 'limit';
    }
  },
  [SET_ORDERS](state, payload) {
    state.orders = payload.orders;
  },
  [SET_ORDER_TYPE](state, payload) {
    state.orderType = payload.orderType;
    if (payload.orderType === 'market') {
      state.orderPrice = null;
    }
  },
  [SET_QUOTE_CURRENCY](state, payload) {
    state.quoteCurrency = payload.quoteCurrency;
  },
  [SET_STATISTIC_CURRENCIES](state, payload) {
    state.statisticCurrencies = payload.statisticCurrencies;
  },
  [SET_USER_EXCHANGES](state, payload) {
    state.userExchanges = payload.userExchanges
  },
  [UPDATE_TICKERS](state, payload) {
    state.tickers = payload.tickers;
  },
};
