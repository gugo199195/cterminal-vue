export default {
  baseCurrencies: (state, getters) => {
    if (getters.exchange) {
      return getters.exchange['baseCurrencies'];
    }

    return [];
  },
  bestBuyPrice: (state) => {
    if (!state.orderBook.bids || !state.orderBook.bids[0]) {
      return 0;
    }

    return state.orderBook.bids[0][0];
  },
  bestSellPrice: (state) => {
    if (!state.orderBook.asks || !state.orderBook.asks[0]) {
      return 0;
    }

    return state.orderBook.asks[0][0];
  },
  commonAccountBalances: (state, getters) => {
    let balances = {};
    Object.keys(getters.ratedBalances).forEach((key) => {
      balances[key] = {};
      state.statisticCurrencies.forEach((quote) => {
        balances[key][quote] = {
          'all': 0,
          'hold': 0,
        };
      });

      getters.ratedBalances[key].forEach((balance) => {
        state.statisticCurrencies.forEach((quote) => {

          balances[key][quote]['all'] += balance[quote].price * balance.amount_all;
          balances[key][quote]['hold'] += balance[quote].price * balance.amount_hold;
        });
      });
    });

    return balances;
  },
  commonBalances: (state, getters) => {
    let balances = {};

    state.statisticCurrencies.forEach((quote) => {
      balances[quote] = 0;
    });

    Object.keys(getters.commonAccountBalances).forEach((key) => {
      Object.keys(getters.commonAccountBalances[key]).forEach((quote) => {
        balances[quote] += getters.commonAccountBalances[key][quote]['all'];
      });
    });

    return balances;
  },
  exchange: state => {
    if (state.exchangeCode) {
      return state.exchanges[state.exchangeCode];
    };

    return null;
  },
  exchangesList: state => {
    return Object.keys(state.exchanges).map((key) => {
      return {
        id: state.exchanges[key].id,
        code: state.exchanges[key].code,
        name: state.exchanges[key].name,
      }
    });
  },
  markets: (state, getters) => {
    if (getters.exchange) {
      let markets = getters.exchange['markets'];

      markets = markets.filter((market) => {
        return !state.baseCurrency || market.base === state.baseCurrency;
      }).filter((market) => {
        return !state.quoteCurrency || market.quote === state.quoteCurrency;
      });

      if (state.tickers) {
        markets = markets.map((market) => {
          return {
            ...market,
            ticker: state.tickers[market.id] || {}
          };
        });
      }

      return markets;
    }

    return [];
  },
  openedOrders: state => {
    let orders = state.orders;

    orders = orders.filter((item) => {
      return item.order_status === 'open';
    });

    orders = orders.map((item) => {
      let account = state.accounts[item.user_exchange_id];
      if (account) {
        item.account_name = account.account_name;
        item.exchange_name = account.exchange_name;
      }

      return item;
    })

    return orders;
  },
  orderMarket: (state) => {
    let orderMarket = null;

    if (!state.market) {
      return orderMarket;
    }

    if (!state.orderAccount) {
      return orderMarket;
    }

    let symbol = state.market.symbol;

    state.exchanges[state.orderAccount.exchange_code].markets.forEach((market) => {
      if (market.symbol === symbol) {
        orderMarket = market;
      }
    });

    return orderMarket;
  },
  orderCost: (state, getters) => {
    let orderCost = null;
    let orderPrice = (state.orderType === 'limit') ? state.orderPrice : getters.orderMarketPrice

    if (
      state.orderAmount &&
      orderPrice
    ) {
      orderCost = orderPrice * state.orderAmount;
    }

    if (orderCost) {
      return orderCost.toFixed(getters.orderMarket.precision.quote);
    }

    return null;
  },
  orderMarketBase: (state, getters) => {
    if (!getters.orderMarket) {
      return null;
    }

    return getters.orderMarket.base;
  },
  marketBase: (state) => {
    if (!state.market) {
      return null;
    }

    return state.market.base;
  },
  marketQuote: (state) => {
    if (!state.market) {
      return null;
    }

    return state.market.quote;
  },
  pairAccounts: (state) => {
    let accounts = [];
    if (!state.market) {
      return accounts;
    }
    let symbol = state.market.symbol;

    Object.keys(state.accounts).forEach((key) => {
      let markets = state.exchanges[state.accounts[key].exchange_code].markets;

      let acceptedMarkets = markets.filter((market) => {
        return market.symbol === symbol;
      });

      if (acceptedMarkets.length > 0) {
        accounts.push(state.accounts[key]);
      }
    });

    return accounts;
  },
  ratedBalances: (state) => {
    let ratedBalances = {};

    if (!state.balances || !state.tickers) {
      return ratedBalances;
    }

    Object.keys(state.balances).forEach((key) => {
      if (state.accounts[key]) {
        ratedBalances[key] = []

        state.balances[key].forEach((item) => {
          ratedBalances[key].push({...item});
        })

        let exchangeCode = state.accounts[key].exchange_code;

        ratedBalances[key].map((balance) => {
          let chains = state.exchanges[exchangeCode].rateChains[balance.asset]
          if (chains) {
            Object.keys(chains).forEach((quote) => {
              let price = 1;
              let changeMultiplier = 1;

              chains[quote].forEach((link) => {
                if (link.m && state.tickers[link.m]) {
                  if (link.d) {
                    price = price * state.tickers[link.m].p;
                    changeMultiplier *= (state.tickers[link.m].c / 100) + 1;
                  } else {
                    price = price / state.tickers[link.m].p;
                    changeMultiplier *= (-1 * state.tickers[link.m].c / 100) + 1;
                  }
                }
              })
              balance[quote] = {
                price: price,
                percentage: ((changeMultiplier - 1) * 100).toFixed(3),
              };
            })
          } else {
            state.statisticCurrencies.forEach((quote) => {
              balance[quote] = {
                price: 0,
                percentage: 0,
              };
            });
          }
        });
      }
    })

    return ratedBalances
  },
  quoteCurrencies: (state, getters) => {
    if (getters.exchange) {
      return getters.exchange['quoteCurrencies'];
    }

    return [];
  },
  buyEnabled: (state, getters) => {
    if (!getters.orderMarket || !getters.orderCost || !state.balances[state.orderAccount.id]) {
      return false;
    }

    let balanceFree = 0;
    state.balances[state.orderAccount.id].forEach((balance) => {
      if (balance.asset === getters.orderMarket.quote) {
        balanceFree = balance.amount_free;
      }
    });

    if (getters.orderCost < 0 || getters.orderCost > balanceFree) {
      return false;
    }

    let limits = getters.orderMarket.limits;

    if (limits.price.min && limits.price.min > state.orderPrice) {
      return false;
    }

    if (limits.price.max && limits.price.max < state.orderPrice) {
      return false;
    }

    if (limits.amount.min && limits.amount.min > state.orderAmount) {
      return false;
    }

    if (limits.amount.max && limits.amount.max < state.orderAmount) {
      return false;
    }

    if (limits.cost.min && limits.cost.min > getters.orderCost) {
      return false;
    }

    if (limits.cost.max && limits.cost.max < getters.orderCost) {
      return false;
    }

    return true;
  },
  sellEnabled: (state, getters) => {
    if (!getters.orderMarket || !getters.orderCost) {
      return false;
    }

    let balanceFree = 0;
    if(state.balances.length != 0) {
      state.balances[state.orderAccount.id].forEach((balance) => {
        if (balance.asset === getters.orderMarket.base) {
          balanceFree = balance.amount_free;
        }
      });
    }


    if (state.orderAmount < 0 || state.orderAmount > balanceFree) {
      return false;
    }

    return true;
  },
  marketTicker: (state) => {
    let voidTicker = {
      c: 0,
      p: 0,
      v: 0,
      h: 0,
      l: 0,
    };
    if (!state.tickers) {
      return voidTicker;
    }
    if (!state.tickers[state.market.id]) {
      return voidTicker;
    }

    return state.tickers[state.market.id];
  },
  orderMarketPrice: (state, getters) => {
    if (!state.tickers) {
      return 0;
    }
    if (!getters.orderMarket) {
      return 0;
    }
    if (!state.tickers[getters.orderMarket.id]) {
      return 0;
    }

    return state.tickers[getters.orderMarket.id].p;
  },
  summaryAccountBalancesHistory: (state, getters) => {
    let result = {};

    state.statisticCurrencies.forEach((quote) => {
      result[quote] = {
        acquisitionCost: 0,
        profitLoss: {
          value: 0,
          percentage: 0,
        },
        holding: 0,
        change24h: {
          value: 0,
          percentage: 0,
        },
      };
    });

    if (!state.balanceAccount) {
      return result;
    }

    return getters.summaryBalancesHistory[state.balanceAccount.id];
  },
  summaryBalancesHistory: (state, getters) => {
    let result = {};

    Object.keys(state.accounts).forEach((accountId) => {
      let accountResult = {};

      state.statisticCurrencies.forEach((quote) => {
        accountResult[quote] = {
          acquisitionCost: 0,
          profitLoss: {
            value: 0,
            percentage: 0,
          },
          holding: 0,
          change24h: {
            value: 0,
            percentage: 0,
          },
        };
      });

      let balanceHistory = state.balancesHistory[accountId];
      let ratedBalance = getters.ratedBalances[accountId];

      if (!balanceHistory || !ratedBalance) {
        result[accountId] = accountResult;
        return;
      }

      Object.keys(balanceHistory).forEach((currency) => {
        balanceHistory[currency].forEach((balanceEvent) => {
          state.statisticCurrencies.forEach((quote) => {
            accountResult[quote]['acquisitionCost'] += balanceEvent.amount_change * balanceEvent['rates'][quote];
          });
        });
      });

      state.statisticCurrencies.forEach((quote) => {
        accountResult[quote]['holding'] = getters.commonAccountBalances[accountId][quote]['all'];
        accountResult[quote]['profitLoss']['value'] = accountResult[quote]['holding'] - accountResult[quote]['acquisitionCost'];
        accountResult[quote]['profitLoss']['percentage'] = (accountResult[quote]['holding'] - accountResult[quote]['acquisitionCost']) / accountResult[quote]['acquisitionCost'] * 100;

        let presentVolume = 0;
        let pastVolume = 0;

        ratedBalance.forEach((balance) => {
          let balancePresentVolume = balance[quote].price * balance.amount_all;
          let balancePastVolume = (balancePresentVolume * 100) / (parseFloat(balance[quote].percentage) + 100);

          presentVolume += balancePresentVolume;
          pastVolume += balancePastVolume;
        });

        accountResult[quote]['change24h']['value'] = presentVolume - pastVolume;
        accountResult[quote]['change24h']['percentage'] = (presentVolume - pastVolume) / pastVolume * 100;
      });

      result[accountId] = accountResult;
    })

    return result;
  },
  summaryAssetBalancesHistory: (state, getters) => {
    let result = {};

    Object.keys(state.accounts).forEach((accountId) => {
      let assetResult = {};

      let ratedBalance = getters.ratedBalances[accountId];
      let balanceHistory = state.balancesHistory[accountId];

      if (!balanceHistory || !ratedBalance) {
        result[accountId] = assetResult;

        return;
      }

      ratedBalance.forEach((balance) => {
        assetResult[balance.asset] = {};

        state.statisticCurrencies.forEach((quote) => {
          assetResult[balance.asset][quote] = {
            acquisitionCost: 0,
            profitLoss: {
              value: 0,
              percentage: 0,
            },
            holding: 0,
            change24h: {
              value: 0,
              percentage: 0,
            },
          };
        });

        let assetBalanceHistory = balanceHistory[balance.asset];

        if (!assetBalanceHistory) {
          result[accountId] = assetResult;

          return;
        }

        assetBalanceHistory.forEach((balanceEvent) => {
          state.statisticCurrencies.forEach((quote) => {
            assetResult[balance.asset][quote]['acquisitionCost'] += balanceEvent.amount_change * balanceEvent['rates'][quote];
          });
        });

        state.statisticCurrencies.forEach((quote) => {
          assetResult[balance.asset][quote]['holding'] = balance.amount_all * balance[quote].price;

          assetResult[balance.asset][quote]['profitLoss']['value'] = assetResult[balance.asset][quote]['holding'] - assetResult[balance.asset][quote]['acquisitionCost'];

          assetResult[balance.asset][quote]['profitLoss']['percentage'] = (assetResult[balance.asset][quote]['holding'] - assetResult[balance.asset][quote]['acquisitionCost']) / assetResult[balance.asset][quote]['acquisitionCost'] * 100;

          let balancePastVolume = (assetResult[balance.asset][quote]['holding'] * 100) / (parseFloat(balance[quote].percentage) + 100);

          assetResult[balance.asset][quote]['change24h']['value'] = assetResult[balance.asset][quote]['holding'] - balancePastVolume;
          assetResult[balance.asset][quote]['change24h']['percentage'] = (assetResult[balance.asset][quote]['holding'] - balancePastVolume) / balancePastVolume * 100;
        });
      });

      result[accountId] = assetResult;
    })

    return result;
  },
}
