import binance from './ws-connectors/binance';
import huobipro from './ws-connectors/huobipro';
import hitbtc from './ws-connectors/hitbtc';
import okex from './ws-connectors/okex';
import poloniex from './ws-connectors/poloniex';

let exchanges = {
    binance: binance,
    huobipro: huobipro,
    hitbtc: hitbtc,
    okex: okex,
    poloniex: poloniex,
};
let orderBookConnector = null;

export default {
    setOrderBookConnector: function (store, market) {
        if (orderBookConnector) {
            orderBookConnector.killOrderBookConnector();
        }

        if (store.state.tradingManager.market) {
            orderBookConnector = exchanges[store.state.tradingManager.market.exchange_code];
            if (orderBookConnector) {
                orderBookConnector.setOrderBookConnector(store);
            }
        }
    }
}