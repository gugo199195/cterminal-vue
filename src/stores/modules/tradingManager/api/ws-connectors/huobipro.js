import pako from 'pako';

const wssUrl = 'wss://api.huobi.pro/ws';

export default {
    name: 'huobipro',
    orderBookWs: null,

    setOrderBookConnector(store) {
        let market = store.state.tradingManager.market;
        let pair = market.base + market.quote;

        this.orderBookWs = new WebSocket(wssUrl);
        this.orderBookWs.binaryType = "arraybuffer";

        this.orderBookWs.onopen = () => {
            this.orderBookWs.send(JSON.stringify({
                "sub": 'market.' + pair.toLowerCase() + '.depth.step0',
                "id": '1'
            }));
        };
        store.$axios.get('cabinet/order-book-snapshot/' + market.id).then((response) => {
            store.dispatch('setOrderBook', {orderBook: response.data.data});

            this.orderBookWs.onmessage = (data) => {
                let text = pako.inflate(data.data, {
                    to: 'string'
                });
                let msg = JSON.parse(text);

                let orderBook = {
                    asks: [],
                    bids: [],
                }

                if (msg.ping) {
                    this.orderBookWs.send(JSON.stringify({
                        pong: msg.ping
                    }));

                    return;
                } else if (msg.tick) {
                    orderBook = {
                        asks: msg.tick.asks,
                        bids: msg.tick.bids,
                    }
                }

                store.dispatch('setOrderBook', {orderBook: orderBook})
            };
        });
    },

    killOrderBookConnector() {
        if (this.orderBookWs !== null) {
            this.orderBookWs.close();
            this.orderBookWs = null;
        }
    }
}
