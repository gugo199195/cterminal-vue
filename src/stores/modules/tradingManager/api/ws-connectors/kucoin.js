export default {
    name: 'kucoin',
    ws: null,
    ping: null,
    batchWorker: null,
    batchTickers: {},
    async setExchangeTickersConnector(store) {
        const { data } = await store.$axios.get('cabinet/kucoin-ws-token/');
        let token = data.data.token;

        let wssUrl = 'wss://push1.kucoin.com/endpoint?bulletToken=' + token + '&format=json&resource=api';

        this.ws = new WebSocket(wssUrl);

        this.ws.onopen = () => {
            let k = 2;
            store.state.tradingManager.exchanges[this.name].quoteCurrencies.forEach((item) => {
                this.ws.send(JSON.stringify({
                    "id": k,
                    "type": "subscribe",
                    "topic": "/market/" + item,
                }));
                k++;
            });

            this.ping = setInterval(() => {
                this.ws.send(JSON.stringify({
                    "id": "1",
                    "type": "ping"
                }));
            }, 40000)
        };

        this.ws.onmessage = (msg) => {
            let ticker = JSON.parse(msg.data).data;
            if (!ticker) {
                return;
            }
            this.batchTickers[ticker.symbol] = {
                high: ticker.high,
                last: ticker.lastDealPrice,
                low: ticker.low,
                percentage: (ticker.changeRate * 100).toFixed(3),
                volume: ticker.volValue,
            };
        };

        this.batchWorker = setInterval(() => {
            store.dispatch('updateTickers', {exchange: this.name, tickers: this.batchTickers});
            this.batchTickers = {};
        }, store.state.tradingManager.tickerRefreshRate);
    },
    killExchangeTickersConnector() {
        if (this.ws !== null) {
            this.ws.close();
            this.ws = null;
        }
        if (this.ping !== null) {
            clearInterval(this.ping);
            this.ping = null;
        }
        if (this.batchWorker !== null) {
            clearInterval(this.batchWorker);
            this.batchWorker = null;
        }
        this.batchTickers = {};
    }
}
