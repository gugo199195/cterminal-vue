const wssUrl = 'wss://cterminal.local/ws/';
const exchangeCode = 'exmo';

export default {
    name: 'exmo',
    ws: null,
    batchWorker: null,
    batchTickers: {},
    setExchangeTickersConnector(store) {
        this.ws = new WebSocket(wssUrl);
        this.ws.onopen = () => {
            this.ws.send(JSON.stringify({
                "exchange": exchangeCode,
                "channel": "tickers"
            }));
        };
        this.ws.onmessage = (msg) => {
            let data = JSON.parse(msg.data);

            this.batchTickers = {...this.batchTickers, ...data.data}
        };

        this.batchWorker = setInterval(() => {
            store.dispatch('updateTickers', {exchange: this.name, tickers: this.batchTickers});
            this.batchTickers = {};
        }, store.state.tradingManager.tickerRefreshRate);
    },
    killExchangeTickersConnector() {
        if (this.ws !== null) {
            this.ws.close();
            this.ws = null;
        }
        if (this.batchWorker !== null) {
            clearInterval(this.batchWorker);
            this.batchWorker = null;
        }
        this.batchTickers = {};
    }
}
