const wssUrl = 'wss://api2.poloniex.com';

export default {
    name: 'poloniex',
    orderBookWs: null,
    batchWorker: null,

    setOrderBookConnector(store) {
        let market = store.state.tradingManager.market;
        let pair = market.ticker_code;

        this.orderBookWs = new WebSocket(wssUrl);

        this.orderBookWs.onopen = () => {
            this.orderBookWs.send(JSON.stringify({
                "command": "subscribe",
                "channel": pair
            }));
        };

        let diff = {
            asks: [],
            bids: [],
        };

        this.orderBookWs.onmessage = (data) => {
            let msg = JSON.parse(data.data);

            if (!msg[2]) {
                return
            }
            msg[2].forEach(function(msgData) {
                if (msgData[0] === "i") {
                    let orderBook = {
                        asks: [],
                        bids: [],
                    };

                    let asksKeys = Object.keys(msgData[1].orderBook[0]);
                    asksKeys.length = 100;
                    asksKeys.forEach(function (key) {
                        orderBook.asks.push([key, msgData[1].orderBook[0][key]]);
                    });

                    let bidsKeys = Object.keys(msgData[1].orderBook[1]);
                    bidsKeys.length = 100;
                    bidsKeys.forEach(function (key) {
                        orderBook.bids.push([key, msgData[1].orderBook[1][key]]);
                    });

                    store.dispatch('setOrderBook', {orderBook: orderBook});
                } else if (msgData[0] === "o") {
                    if (msgData[1] === 0) {
                        diff.asks.push([msgData[2], msgData[3]]);
                    } else if (msgData[1] === 1) {
                        diff.bids.push([msgData[2], msgData[3]]);
                    }
                }
            });
        };

        this.batchWorker = setInterval(() => {
            store.dispatch('updateOrderBook', {diff: diff});
            diff = {
                asks: [],
                bids: [],
            };
        }, 1000);
    },
    
    killOrderBookConnector() {
        if (this.orderBookWs !== null) {
            this.orderBookWs.close();
            this.orderBookWs = null;
        }
        if (this.batchWorker !== null) {
            clearInterval(this.batchWorker);
            this.batchWorker = null;
        }
    }
}
