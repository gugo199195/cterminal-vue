const wssUrl = 'wss://api.hitbtc.com/api/2/ws';

export default {
    name: 'hitbtc',
    orderBookWs: null,
    batchWorker: null,

    setOrderBookConnector(store) {
        let market = store.state.tradingManager.market;
        let pair = market.ticker_code;

        this.orderBookWs = new WebSocket(wssUrl);

        this.orderBookWs.onopen = () => {
            this.orderBookWs.send(JSON.stringify({
                "method": "subscribeOrderbook",
                "params": {
                    "symbol": pair
                },
                "id": 2
            }));
        };

        let diff = {
            asks: [],
            bids: [],
        };

        this.orderBookWs.onmessage = (data) => {
            let msg = JSON.parse(data.data);

            if (!msg.method) {
                return
            }

            if (msg.method === "snapshotOrderbook") {
                let asks = msg.params.ask;
                let bids = msg.params.bid;
                asks.length = 100;
                bids.length = 100;
                let orderBook = {
                    asks: asks.map((item) => {
                        return [item.price, item.size];
                    }),
                    bids: bids.map((item) => {
                        return [item.price, item.size];
                    }),
                };

                store.dispatch('setOrderBook', {orderBook: orderBook});
            }

            if (msg.method === "updateOrderbook") {
                let asks = msg.params.ask;
                let bids = msg.params.bid;

                diff.asks = diff.asks.concat(asks.map((item) => {
                    return [item.price, item.size];
                }));
                diff.bids = diff.bids.concat(bids.map((item) => {
                    return [item.price, item.size];
                }));
            }
        };

        this.batchWorker = setInterval(() => {
            store.dispatch('updateOrderBook', {diff: diff});
            diff = {
                asks: [],
                bids: [],
            };
        }, 1000);
    },

    killOrderBookConnector() {
        if (this.orderBookWs !== null) {
            this.orderBookWs.close();
            this.orderBookWs = null;
        }
        if (this.batchWorker !== null) {
            clearInterval(this.batchWorker);
            this.batchWorker = null;
        }
    }
}
