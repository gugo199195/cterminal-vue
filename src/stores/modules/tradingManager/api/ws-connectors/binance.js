const wssUrl = 'wss://stream.binance.com:9443/ws/';

export default {
    name: 'binance',
    orderBookWs: null,

    setOrderBookConnector(store) {
        let market = store.state.tradingManager.market;
        let pair = market.base + market.quote;
        let wssOrderBookUrl = wssUrl + pair.toLowerCase() + '@depth';
        let priceTypes = [
            {code: 'b', name: 'bids'},
            {code: 'a', name: 'asks'}
        ];
        this.orderBookWs = new WebSocket(wssOrderBookUrl);

        store.$axios.get('cabinet/order-book-snapshot/' + market.id).then((response) => {

            store.dispatch('setOrderBook', {orderBook: response.data.data})

            this.orderBookWs.onmessage = (msg) => {
                let changes = JSON.parse(msg.data)

                let diff = {
                    bids: [],
                    asks: [],
                };

                priceTypes.forEach((price) => {
                    diff[price.name] = changes[price.code].map((item) => {
                        return [item[0], item[1]];
                    });
                });

                store.dispatch('updateOrderBook', {diff: diff})
            }
        });
    },

    killOrderBookConnector() {
        if (this.orderBookWs !== null) {
            this.orderBookWs.close();
            this.orderBookWs = null;
        }
    }
}
