const wssUrl = 'wss://www.bitmex.com/realtime';

export default {
    name: 'bitmex',
    ws: null,
    batchWorker: null,
    batchTickers: {},
    setExchangeTickersConnector(store) {
        let url = wssUrl + '?subscribe=instrument';
        this.ws = new WebSocket(url);
        this.ws.onerror = () => {
            setTimeout(() => {
                this.ws = new WebSocket(url);
            }, 1000);
        };

        this.ws.onmessage = (msg) => {
            let data = JSON.parse(msg.data);
            let storedTickers = store.state.tradingManager.exchanges[this.name]['tickers'];
            let tickers = {};
            let tickersUpdated = false;
            if (!data.data) {
                return;
            }
            data.data.forEach((item) => {
                let storedTicker = {...storedTickers[item.symbol]};
                if (!storedTicker) {
                    return;
                }

                if (item.highPrice) {
                    storedTicker.high = item.highPrice;
                }
                if (item.lastPrice) {
                    storedTicker.last = item.lastPrice;
                }
                if (item.lowPrice) {
                    storedTicker.low = item.lowPrice;
                }
                if (item.lastChangePcnt) {
                    storedTicker.percentage = (item.lastChangePcnt * 100).toFixed(3);
                }
                if (item.volume24h) {
                    storedTicker.volume = item.volume24h;
                }

                this.batchTickers[item.symbol] = storedTicker;
            });
        };


        this.batchWorker = setInterval(() => {
            store.dispatch('updateTickers', {exchange: this.name, tickers: this.batchTickers});
            this.batchTickers = {};
        }, store.state.tradingManager.tickerRefreshRate);
    },
    killExchangeTickersConnector() {
        if (this.ws !== null) {
            this.ws.close();
            this.ws = null;
        }
        if (this.batchWorker !== null) {
            clearInterval(this.batchWorker);
            this.batchWorker = null;
        }
        this.batchTickers = {};
    }
}
