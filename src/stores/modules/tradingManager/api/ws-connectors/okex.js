import pako from 'pako';
const wssUrl = 'wss://real.okex.com:10440/ws/v1';

export default {
    name: 'okex',
    orderBookWs: null,
    wsPing: null,

    setOrderBookConnector(store) {
        let market = store.state.tradingManager.market;
        let pair = market.base.toLowerCase() + "_" + market.quote.toLowerCase();
        let orderBookInitialized = false;
        let channel = "ok_sub_spot_" + pair + "_depth";

        this.orderBookWs = new WebSocket(wssUrl);
        this.orderBookWs.binaryType = "arraybuffer";

        this.orderBookWs.onopen = () => {
            this.orderBookWs.send(JSON.stringify({
                "event": "addChannel",
                "channel": channel,
            }));

            this.wsPing = setInterval(() => {
                this.orderBookWs.send(JSON.stringify({
                    "event": "ping"
                }));
            }, 20000)
        };

        this.orderBookWs.onmessage = (data) => {
            let text = pako.inflateRaw(data.data, {
                to: 'string'
            });

            let msg = JSON.parse(text);
            if (!msg) {
                return
            }
            msg = msg[0];

            if (!msg || !msg.channel || msg.channel !== channel) {
                return
            }

            if (!orderBookInitialized) {
                store.dispatch('setOrderBook', {orderBook: {
                    asks: msg.data.asks,
                    bids: msg.data.bids,
                }});

                orderBookInitialized = true;
            } else {
                store.dispatch('updateOrderBook', {diff: {
                    asks: msg.data.asks,
                    bids: msg.data.bids,
                }});
            }
        };
    },

    killOrderBookConnector() {
        if (this.orderBookWs !== null) {
            this.orderBookWs.close();
            this.orderBookWs = null;
        }
        if (this.wsPing !== null) {
            clearInterval(this.wsPing);
            this.wsPing = null;
        }
    }
}
