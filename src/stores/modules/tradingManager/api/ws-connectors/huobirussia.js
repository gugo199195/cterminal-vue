import pako from 'pako';

const wssUrl = 'wss://www.huobi.com.ru/api/ws';

export default {
    name: 'huobipro',
    ws: null,
    batchWorker: null,
    batchTickers: {},
    setExchangeTickersConnector(store) {
        this.ws = new WebSocket(wssUrl);
        this.ws.binaryType = "arraybuffer";
        this.ws.onopen = () => {
            this.ws.send(JSON.stringify({
                "sub": 'market.tickers',
                "id": '1'
            }));
        };
        this.ws.onmessage = (msg) => {
            let text = pako.inflate(msg.data, {
                to: 'string'
            });
            let data = JSON.parse(text);
            if (data.ping) {
                this.ws.send(JSON.stringify({
                    pong: data.ping
                }));
            } else if (data.ch === 'market.tickers'){
                let tickers = data.data.reduce((res, item) => {
                    res[item.symbol] = {
                        high: item.high,
                        last: item.close,
                        low: item.low,
                        percentage: ((item.close - item.open) / item.open * 100).toFixed(3),
                        volume: item.amount,
                    };
                    return res;
                }, {});

                this.batchTickers = {...this.batchTickers, ...tickers};
            }
        };

        this.batchWorker = setInterval(() => {
            store.dispatch('updateTickers', {exchange: this.name, tickers: this.batchTickers});
            this.batchTickers = {};
        }, store.state.tradingManager.tickerRefreshRate);
    },
    killExchangeTickersConnector() {
        if (this.ws !== null) {
            this.ws.close();
            this.ws = null;
        }
        if (this.batchWorker !== null) {
            clearInterval(this.batchWorker);
            this.batchWorker = null;
        }
        this.batchTickers = {};
    }
}
