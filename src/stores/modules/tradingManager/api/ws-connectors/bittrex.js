const wssUrl = 'wss://cterminal.local/ws/';
const exchangeCode = 'bittrex';

export default {
    name: 'bittrex',
    ws: null,
    batchWorker: null,
    batchTickers: {},
    setExchangeTickersConnector(store) {
        this.ws = new WebSocket(wssUrl);
        this.ws.onopen = () => {
            this.ws.send(JSON.stringify({
                "exchange": exchangeCode,
                "channel": "tickers"
            }));
        };
        this.ws.onmessage = (msg) => {
            let ticker = JSON.parse(msg.data);
            this.batchTickers[ticker.pair] = ticker.data;
        };

        this.batchWorker = setInterval(() => {
            store.dispatch('updateTickers', {exchange: this.name, tickers: this.batchTickers});
            this.batchTickers = {};
        }, store.state.tradingManager.tickerRefreshRate);
    },
    killExchangeTickersConnector() {
        if (this.ws !== null) {
            this.ws.close();
            this.ws = null;
        }
        if (this.batchWorker !== null) {
            clearInterval(this.batchWorker);
            this.batchWorker = null;
        }
        this.batchTickers = {};
    }
}
