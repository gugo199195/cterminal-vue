import axios from 'axios'

import types, {
  SET_ACCOUNTS,
  SET_BALANCE_ACCOUNT,
  SET_BALANCES,
  SET_BALANCES_HISTORY,
  SET_BASE_CURRENCY,
  SET_EXCHANGE_CODE,
  SET_EXCHANGE_CURRENCY_CODES,
  SET_EXCHANGES,
  SET_FAVORITE_EXCHANGES,
  SET_FAVORITE_MARKETS,
  SET_FAVORITE_TICKERS,
  SET_MARKET,
  SET_ORDER_ACCOUNT,
  SET_ORDER_BOOK,
  UPDATE_ORDER_BOOK,
  SET_ORDER_AMOUNT,
  SET_ORDER_PRICE,
  SET_ORDERS,
  SET_ORDER_TYPE,
  SET_QUOTE_CURRENCY,
  SET_STATISTIC_CURRENCIES,
  SET_USER_EXCHANGES,
  UPDATE_TICKERS,
} from './api/mutation-types';

import connector from "./api/ws-connector";

export default {
  setBalanceAccount({commit}, {balanceAccount}) {
    commit(types.SET_BALANCE_ACCOUNT, {balanceAccount: balanceAccount});
  },
  setBalances({commit}, {balances}) {
    commit(types.SET_BALANCES, {balances: balances});
  },
  setBalancesHistory({commit}, {balancesHistory}) {
    commit(types.SET_BALANCES_HISTORY, {balancesHistory: balancesHistory});
  },
  setBaseCurrency({commit}, {baseCurrency}) {
    commit(types.SET_BASE_CURRENCY, {baseCurrency: baseCurrency});
  },
  setExchangeCode({commit}, {exchangeCode}) {
    commit(types.SET_EXCHANGE_CODE, {exchangeCode: exchangeCode});
  },
  setMarket({commit, dispatch}, {market}) {
    commit(types.SET_ORDER_ACCOUNT, {orderAccount: null});
    commit(types.SET_MARKET, {market: market});
    commit(types.SET_ORDER_PRICE, {orderPrice: null});
    commit(types.SET_ORDER_AMOUNT, {orderAmount: null});
    dispatch('setOrderBookConnector');
  },
  setOrderAmount({commit}, {orderAmount}) {
    commit(types.SET_ORDER_AMOUNT, {orderAmount: orderAmount});
  },
  setOrderPrice({commit}, {orderPrice}) {
    commit(types.SET_ORDER_PRICE, {orderPrice: orderPrice});
  },
  setOrders({commit}, {orders}) {
    commit(types.SET_ORDERS, {orders: orders});
  },
  setOrderAccount({commit}, {orderAccount}) {
    commit(types.SET_ORDER_ACCOUNT, {orderAccount: orderAccount});
  },
  setOrderBookConnector() {
    connector.setOrderBookConnector(this);
  },
  setOrderBook({commit}, {orderBook}) {
    commit(types.SET_ORDER_BOOK, {orderBook: orderBook});
  },
  setOrderType({commit}, {orderType}) {
    commit(types.SET_ORDER_TYPE, {orderType: orderType});
  },
  updateOrderBook({commit}, {diff}) {
    commit(types.UPDATE_ORDER_BOOK, {diff: diff});
  },
  setQuoteCurrency({commit}, {quoteCurrency}) {
    commit(types.SET_QUOTE_CURRENCY, {quoteCurrency: quoteCurrency});
  },
  tradingInit({commit, dispatch}) {
    axios.get('https://back.hicotrading.com/api/v1/cabinet/trading-init').then((response) => {
      let data = response.data.data;

      commit(types.SET_EXCHANGES, {exchanges: data.exchanges});
      commit(types.SET_USER_EXCHANGES, {exchanges: data.userExchanges});
      commit(types.SET_ACCOUNTS, {accounts: data.accounts});
      commit(types.SET_FAVORITE_MARKETS, {favoriteMarkets: data.favorites.favoriteMarkets});
      commit(types.SET_FAVORITE_EXCHANGES, {favoriteExchanges: data.favorites.favoriteExchanges});
      commit(types.SET_FAVORITE_TICKERS, {favoriteTickers: data.favorites.favoriteTickers});
      commit(types.SET_ORDERS, {orders: data.orders});
      commit(types.SET_STATISTIC_CURRENCIES, {statisticCurrencies: data.statisticCurrencies});
      commit(types.SET_EXCHANGE_CURRENCY_CODES, {exchangeCurrencyCodes: data.exchangeCurrencyCodes});
      commit(types.SET_BALANCES, {balances: data.balances});
      commit(types.SET_BALANCES_HISTORY, {balancesHistory: data.balancesHistory});

      dispatch('updateTickers', {tickers: data.tickers});

      window.Echo.channel('info')
        .listen('.tickers_updated', (e) => {
          dispatch('updateTickers', {tickers: e.tickers});
        })
    });
  },
  updateTickers({commit}, {tickers}) {
    commit(types.UPDATE_TICKERS, {tickers: tickers});
  },
}
