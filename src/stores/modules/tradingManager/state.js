export default {
  accounts: {},
  Gurgen: "ASDASDASD",
  balanceAccount: null,
  balances: {},
  balancesHistory: {},
  baseCurrency: null,
  exchangeCode: null,
  exchangeCurrencyCodes: {},
  exchanges: [],
  favoriteExchanges: [],
  favoriteMarkets: [],
  favoriteTickers: [],
  market: null,
  orderAccount: null,
  orderAmount: null,
  orderBook: {
    asks: [],
    bids: [],
  },
  orderPrice: null,
  orders: [],
  orderType: 'market',
  quoteCurrency: null,
  statisticCurrencies: [],
  tickerRefreshRate: 3000,
  tickers: null,
  userExchanges: [],
}
