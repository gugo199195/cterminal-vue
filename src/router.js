import Vue from 'vue'
import Router from 'vue-router'

import Default from "./layouts/default"

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      name: 'home',
      path: '/',
      meta: {layout: "default"},
      component: () => import('./pages/home.vue')
    },
    {
      name: 'login',
      path: '/login',
      meta: {layout: "default"},
      component: () => import('./pages/login.vue')
    },
    {
      name: 'registration',
      path: '/registration',
      component: () => import('./pages/registration.vue')
    },
    {
      name: 'resetPassword',
      path: '/reset-password',
      component: () => import('./pages/resetPassword.vue')
    },
    {
      name: 'resetPasswordConfirmation',
      path: '/reset-password/:code',
      component: () => import('./pages/resetPasswordConfirmation.vue')
    }
  ]
})
