import _ from 'lodash';

let EraseControlDirective = {
  bind(el, binding, vnode) {
    el.addEventListener('click', (e) => {
      e.preventDefault();
      if (_.has(vnode.context, binding.expression)) {
        _.set(vnode.context, binding.expression, '');
      }
    });
  },
  update(el, binding, vnode) {
    if (_.has(vnode.context, binding.expression)) {
      let v = _.get(vnode.context, binding.expression, '');
      if (_.isEmpty(v)) {
        el.style.display = 'none';
      } else {
        el.style.display = 'initial';
      }
    }
  },
};

export default EraseControlDirective;
