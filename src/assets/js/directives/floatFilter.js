let FloatFilterDirective = {
    bind: function(el, binding) {
        let inputHandler = function(e) {
            let key = e.which ? e.which : event.keyCode;
            if(key == 44){
                key = 46;
            }
            let ch = String.fromCharCode(key);
            let input = el.getElementsByTagName('input')[0];
            let floatRegexp = /^(\d*([\.]\d*)?)?$/;
            let value = input.value;
            let newValue = '';
            let newSelectionStart = 0;
            let newSelectionEnd = 0;
            let min = binding.value ? binding.value.min : null;
            let max = binding.value ? binding.value.max : null;
            let precision = input.getAttribute('precision');

            if (document.selection) {
                let range = document.selection.createRange();
                range.text = ch;
            } else if (input.selectionStart || input.selectionStart == '0') {
                let start = input.selectionStart;
                let end = input.selectionEnd;
                newValue = value.substring(0, start) + ch + value.substring(end, input.value.length);
                newSelectionStart = start+1;
                newSelectionEnd = start+1;
            } else {
                newValue += ch;
            }

            if (newValue.match(floatRegexp)) {
                let floatValue = parseFloat(newValue);
                let decimalSigns = ~newValue.indexOf('.') ? newValue.substr(newValue.indexOf('.') + 1).length : 0;
                let checkLimits =
                    (!min || floatValue === 0 || floatValue >= min) &&
                    (!max || floatValue <= max) &&
                    (!precision || decimalSigns <= precision);



                if (checkLimits) {
                    input.value = newValue;
                    input.selectionStart = newSelectionStart;
                    input.selectionEnd = newSelectionEnd;

                    if ("createEvent" in document) {
                        let evt = document.createEvent("HTMLEvents");
                        evt.initEvent("input", false, true);
                        input.dispatchEvent(evt);
                    } else {
                        input.fireEvent("oninput");
                    }
                }
            }

            e.preventDefault();
        };

        el.addEventListener("keypress", inputHandler);
    },
};

export default FloatFilterDirective;
