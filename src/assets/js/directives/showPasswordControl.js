let ShowPasswordControlDirective = {
  bind(el, {expression}, vnode) {
    if (process.client) {
      el.addEventListener('click', (e) => {
        e.preventDefault();
        let input = document.getElementById(expression);
        if (input.type === 'password') {
          input.setAttribute('type', 'text');
        } else {
          input.setAttribute('type', 'password');
        }
        input.focus();
      });
    }
  },
};

export default ShowPasswordControlDirective;
