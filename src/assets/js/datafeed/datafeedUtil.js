export default {
    getIntervalSec(resolution) {
        if (resolution === "D") {
            return 60 * 1440;
        }
        if (resolution === "W") {
            return 60 * 1440 * 7;
        }
        if (resolution === "M") {
            return 60 * 1440 * 30;
        }
        if (resolution === "Y") {
            return 60 * 1440 * 365;
        }

        return resolution * 60;
    },
    getCurrentWeekTimestamp() {
        let now = new Date();
        let day = now.getDay();
        let firstDay = now.getDate() - day + (day == 0 ? -6:1);

        return new Date(now.getFullYear(), now.getMonth(), firstDay).getTime();
    },
    getCurrentMonthTimestamp() {
        let now = new Date();

        return new Date(now.getFullYear(), now.getMonth()).getTime();
    }
}