import pako from 'pako';

const wssUrl = 'wss://api.huobi.pro/ws';
const intervalsMap = {
    "1": "1min",
    "5": "5min",
    "15": "15min",
    "30": "30min",
    "60": "60min",
    "D": "1day",
    "W": "1week",
    "M": "1mon",
};

export default {
    barsWs: null,
    lastBar: null,
    intervalsMap: intervalsMap,

    subscribeBars: function (market, resolution, onRealtimeCallback, subscribeUID, onResetCacheNeededCallback) {
        if (this.barsWs) {
            this.barsWs.close();
        }

        let interval = intervalsMap[resolution];
        let channel = 'market.' + market.ticker_code + '.kline.' + interval;

        this.barsWs = new WebSocket(wssUrl);
        this.barsWs.binaryType = "arraybuffer";

        this.barsWs.onopen = () => {
            this.barsWs.send(JSON.stringify({
                "sub": channel,
                "id": '1'
            }));
            console.log('Huobi opened');
        };

        this.barsWs.onclose = () => {
            console.log('Huobi closed');
        };

        this.barsWs.onmessage = (data) => {
            let text = pako.inflate(data.data, {
                to: 'string'
            });
            let msg = JSON.parse(text);

            if (msg.ping) {
                this.barsWs.send(JSON.stringify({
                    pong: msg.ping
                }));

                return;
            } else if (msg.ch === channel && msg.tick) {
                let bar = {
                    time: msg.tick.id * 1000,
                    open: msg.tick.open,
                    high: msg.tick.high,
                    low: msg.tick.low,
                    close: msg.tick.close,
                    volume: msg.tick.vol,
                };

                onRealtimeCallback(bar);
            }
        };

        this.barsWs.onerror = function(error) {
            console.log("Huobi error " + error.message);
        };
    },
}