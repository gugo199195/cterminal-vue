import axios from 'axios'

const wssUrl = "wss://api2.poloniex.com";
const intervalsMap = {
    "5": "300",
    "15": "900",
    "30": "1800",
    "120": "7200",
    "240": "14400",
    "D": "86400",
};

export default {
    barsWs: null,
    lastBar: null,
    intervalsMap: intervalsMap,

    subscribeBars: function (market, resolution, onRealtimeCallback, subscribeUID, onResetCacheNeededCallback, lastBar) {
        if (this.barsWs) {
            this.barsWs.close();
        }

        this.lastBar = lastBar;

        let interval = intervalsMap[resolution];
        let intervalSec = resolution === "D" ? 86400 : resolution * 60;

        axios.get("https://poloniex.com/public?command=returnTicker").then((response) => {

            let marketId = response.data[market.ticker_code]["id"];
            this.barsWs = new WebSocket(wssUrl);

            this.barsWs.onopen = () => {
                this.barsWs.send(JSON.stringify({
                    "command": "subscribe",
                    "channel": 1002
                }));
                console.log('Poloniex opened');
            };

            this.barsWs.onclose = () => {
                console.log('Poloniex closed');
            };

            this.barsWs.onmessage = (msg) => {
                let data = JSON.parse(msg.data);

                if (!data[2] || !(data[2][0] === marketId)) {
                    return;
                }
                let msgPrice = data[2][1];
                let msgTime = Date.now();

                let time = Math.floor(msgTime / 1000);
                let openTime = (time - time % intervalSec) * 1000;

                let bar = {
                    time: null,
                    open: null,
                    high: null,
                    low: null,
                    close: null,
                    volume: null,
                };

                if (this.lastBar.time === openTime) {
                    bar = {
                        time: openTime,
                        open: this.lastBar.open,
                        high: this.lastBar.high < msgPrice ? msgPrice : this.lastBar.high,
                        low: this.lastBar.low > msgPrice ? msgPrice : this.lastBar.low,
                        close: msgPrice,
                        volume: this.lastBar.volume,
                    };
                } else {
                    bar = {
                        time: openTime,
                        open: msgPrice,
                        high: msgPrice,
                        low: msgPrice,
                        close: msgPrice,
                        volume: 0,
                    };
                }

                onRealtimeCallback(bar);
            };

            this.barsWs.onerror = function(error) {
                console.log("Poloniex error " + error.message);
            };
        });
    },
}