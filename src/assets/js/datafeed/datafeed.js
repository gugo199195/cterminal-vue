import binanceProvider from './binanceProvider';
import huobiProvider from './huobiProvider';
import okexProvider from './okexProvider';
import hitbtcProvider from './hitbtcProvider';
import poloniexProvider from './poloniexProvider';
import kucoinProvider from './kucoinProvider';

const config = {
    supported_resolutions: ["5", "15", "30", "60", "120", "240", "D", "W"]
};

const exchangeProviders = {
    binance: binanceProvider,
    huobipro: huobiProvider,
    okex: okexProvider,
    hitbtc: hitbtcProvider,
    poloniex: poloniexProvider,
    kucoin: kucoinProvider,
};

export default {
    market: null,
    store: null,
    provider: null,
    lastBar: null,

    getDatafeed(market, store) {
        this.market = market;
        this.store = store;
        this.provider = exchangeProviders[market.exchange_code];
        this.lastBar = [];
        let supportedResolutions = Object.keys(this.provider.intervalsMap);
        let config = {
            supported_resolutions: supportedResolutions
        };

        return {
            onReady: cb => {
                setTimeout(() => cb(config), 0)
            },
            searchSymbols: (userInput, exchange, symbolType, onResultReadyCallback) => {

            },
            resolveSymbol: (symbolName, onSymbolResolvedCallback, onResolveErrorCallback) => {
                let symbol_stub = {
                    name: this.market.symbol + ' - ' + this.market.exchange.name,
                    description: '',
                    type: 'crypto',
                    session: '24x7',
                    timezone: 'UTC',
                    ticker: this.market.id,
                    minmov: 1,
                    pricescale: 100000000,
                    has_intraday: true,
                    has_daily: true,
                    has_weekly_and_monthly: true,
                    has_empty_bars: true,
                    supported_resolution: supportedResolutions,
                    force_session_rebuild: false,
                    volume_precision: 8,
                    data_status: 'streaming',
                }

                setTimeout(function() {
                    onSymbolResolvedCallback(symbol_stub)
                }, 0)
            },
            getBars: (symbolInfo, resolution, from, to, onHistoryCallback, onErrorCallback, firstDataRequest) => {
                let requestParams = ['interval=' + resolution,];

                if (from) {
                    requestParams[requestParams.length] = 'startTime=' + from;
                }
                if (to) {
                    requestParams[requestParams.length] = 'endTime=' + to;
                }

                this.store.$axios.get('cabinet/bars-snapshot/' + this.market.id + '/?' + requestParams.join('&')).then((response) => {
                    let bars = response.data.data;

                    if (bars.length) {
                        this.lastBar = bars[bars.length - 1];
                        onHistoryCallback(bars, {noData: false});
                    } else {
                        onHistoryCallback(bars, {noData: true});
                    }
                });
            },
            subscribeBars: (symbolInfo, resolution, onRealtimeCallback, subscribeUID, onResetCacheNeededCallback) => {
                this.provider.subscribeBars(this.market, resolution, onRealtimeCallback, subscribeUID, onResetCacheNeededCallback, this.lastBar, this.store.$axios);
            },
            unsubscribeBars: function (subscriberUID) {

            },
            calculateHistoryDepth: function (resolution, resolutionBack, intervalBack) {

            },
            getMarks: function (symbolInfo, from, to, onDataCallback, resolution) {

            },
            getTimescaleMarks: function (symbolInfo, from, to, onDataCallback, resolution) {

            },
            getServerTime: function (callback) {

            },
        }
    }
}
