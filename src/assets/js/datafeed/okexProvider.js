import pako from 'pako';

const wssUrl = 'wss://real.okex.com:10442/ws/v3';
const intervalsMap = {
    "1": "60",
    "3": "180",
    "5": "300",
    "15": "900",
    "30": "1800",
    "60": "3600",
    "120": "7200",
    "240": "14400",
    "360": "21600",
    "720": "43200",
    "D": "86400",
    "W": "604800",
};

export default {
    barsWs: null,
    lastBar: null,
    wsPing: null,
    intervalsMap: intervalsMap,

    subscribeBars: function (market, resolution, onRealtimeCallback, subscribeUID, onResetCacheNeededCallback) {
        if (this.barsWs) {
            this.barsWs.close();
        }

        if (this.wsPing !== null) {
            clearInterval(this.wsPing);
            this.wsPing = null;
        }

        let interval = intervalsMap[resolution];
        let table = 'spot/candle' + interval + 's';
        let channel = table + ':' + market.ticker_code;
        this.barsWs = new WebSocket(wssUrl);
        this.barsWs.binaryType = "arraybuffer";

        this.barsWs.onopen = () => {
            this.barsWs.send(JSON.stringify({
                "op": 'subscribe',
                "args": [channel]
            }));

            this.wsPing = setInterval(() => {
                this.barsWs.send("ping");
            }, 20000)

            console.log('Okex opened');
        };

        this.barsWs.onclose = () => {
            console.log('Okex closed');
        };

        this.barsWs.onmessage = (data) => {

            let msg = pako.inflateRaw(data.data, {
                to: 'string'
            });

            if (msg === "pong") {
                return
            }

            msg = JSON.parse(msg);

            if (!msg || !(msg.table === table)) {
                return;
            }

            let tick = msg.data[0].candle;

            let bar = {
                time: new Date(tick[0]).getTime(),
                open: tick[1],
                high: tick[2],
                low: tick[3],
                close: tick[4],
                volume: tick[5],
            };

            onRealtimeCallback(bar);
        };

        this.barsWs.onerror = function(error) {
            console.log("Okex error " + error.message);
        };
    },
}