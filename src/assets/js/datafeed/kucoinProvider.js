const intervalsMap = {
    "1": "1min",
    "3": "3min",
    "5": "5min",
    "15": "15min",
    "30": "30min",
    "60": "1hour",
    "120": "2hour",
    "240": "4hour",
    "360": "6hour",
    "480": "8hour",
    "720": "12hour",
    "D": "1day",
    "W": "1week"
};

export default {
    barsWs: null,
    lastBar: null,
    wsPing: null,
    intervalsMap: intervalsMap,

    subscribeBars: function (market, resolution, onRealtimeCallback, subscribeUID, onResetCacheNeededCallback, lastBar, axios) {
        if (this.barsWs) {
            this.barsWs.close();
        }

        if (this.wsPing !== null) {
            clearInterval(this.wsPing);
            this.wsPing = null;
        }

        this.lastBar = lastBar;

        let interval = intervalsMap[resolution];
        let intervalSec;
        if (resolution === "D") {
            intervalSec = 86400;
        } else if (resolution === "W") {
            intervalSec = 604800;
        } else {
            intervalSec = resolution * 60;
        }


        axios.get("cabinet/kucoin-ws-data").then((response) => {
            let wssUrl = response.data.data.instanceServers[0].endpoint + "?token=" + response.data.data.token;
            this.barsWs = new WebSocket(wssUrl);

            this.barsWs.onopen = () => {
                this.barsWs.send(JSON.stringify({
                    "id": new Date().getTime(),
                    "type": "subscribe",
                    "topic": "/market/ticker:" + market.ticker_code,
                    "response": true,
                }));

                this.wsPing = setInterval(() => {
                    this.barsWs.send(JSON.stringify({
                        "id": new Date().getTime(),
                        "type": "ping",
                    }));
                }, 40000);

                console.log('Kucoin opened');
            };

            this.barsWs.onclose = (msg) => {
                console.log('Kucoin closed');
            };

            this.barsWs.onmessage = (msg) => {
                let data = JSON.parse(msg.data);

                if (data.subject != "trade.ticker") {
                    return;
                }

                let msgPrice = data.data.price;
                let msgTime = data.data.time;

                let time = Math.floor(msgTime / 1000);
                let openTime = (time - time % intervalSec) * 1000;

                let bar = {
                    time: null,
                    open: null,
                    high: null,
                    low: null,
                    close: null,
                    volume: null,
                };

                if (this.lastBar.time === openTime) {
                    bar = {
                        time: openTime,
                        open: this.lastBar.open,
                        high: this.lastBar.high < msgPrice ? msgPrice : this.lastBar.high,
                        low: this.lastBar.low > msgPrice ? msgPrice : this.lastBar.low,
                        close: msgPrice,
                        volume: this.lastBar.volume,
                    };
                } else {
                    bar = {
                        time: openTime,
                        open: msgPrice,
                        high: msgPrice,
                        low: msgPrice,
                        close: msgPrice,
                        volume: 0,
                    };
                }

                onRealtimeCallback(bar);
            };

            this.barsWs.onerror = function(error) {
                console.log("Kucoin error " + error.message);
            };
        });
    },
}
