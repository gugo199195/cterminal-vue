import datafeedUtil from './datafeedUtil';

const wssUrl = "wss://api.hitbtc.com/api/2/ws";
const intervalsMap = {
    "1": "M1",
    "3": "M3",
    "5": "M5",
    "15": "M15",
    "30": "M30",
    "60": "H1",
    "D": "D1",
    "W": "D7",
    "M": "1M",
};

export default {
    barsWs: null,
    lastBar: null,
    intervalsMap: intervalsMap,

    subscribeBars: function (market, resolution, onRealtimeCallback, subscribeUID, onResetCacheNeededCallback) {
        if (this.barsWs) {
            this.barsWs.close();
        }

        let interval = intervalsMap[resolution];
        let intervalSec = datafeedUtil.getIntervalSec(resolution);

        this.barsWs = new WebSocket(wssUrl);

        this.barsWs.onopen = () => {
            this.barsWs.send(JSON.stringify({
                "method": "subscribeCandles",
                "params": {
                    "symbol": market.ticker_code,
                    "period": interval,
                    "limit": 100
                },
                "id": 1
            }));
            console.log('Hitbtc opened');
        };

        this.barsWs.onclose = () => {
            console.log('Hitbtc closed');
        };

        this.barsWs.onmessage = (msg) => {
            let data = JSON.parse(msg.data);

            if (data.method !== "updateCandles") {
                return;
            }

            let barData = data.params.data[0];

            let bar = {
                time: new Date(barData.timestamp).getTime(),
                open: barData.open,
                high: barData.max,
                low: barData.min,
                close: barData.close,
                volume: barData.volume,
            };

            onRealtimeCallback(bar);
        };

        this.barsWs.onerror = function(error) {
            console.log("Hitbtc error " + error.message);
        };
    },
}