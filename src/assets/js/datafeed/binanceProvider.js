import datafeedUtil from './datafeedUtil';

const wssUrl = 'wss://stream.binance.com:9443/ws/';

const intervalsMap = {
    "1": "1m",
    "3": "3m",
    "5": "5m",
    "15": "15m",
    "30": "30m",
    "60": "1h",
    "120": "2h",
    "240": "4h",
    "360": "6h",
    "480": "8h",
    "720": "12h",
    "D": "1d",
    "W": "1w",
    "M": "1M",
};

export default {
    barsWs: null,
    lastBar: null,
    intervalsMap: intervalsMap,

    subscribeBars: function (market, resolution, onRealtimeCallback, subscribeUID, onResetCacheNeededCallback) {
        if (this.barsWs) {
            this.barsWs.close();
        }

        let interval = intervalsMap[resolution];
        let intervalSec = datafeedUtil.getIntervalSec(resolution);
        let wssBarsUrl = wssUrl + market.ticker_code.toLowerCase() + '@kline_' + interval;

        this.barsWs = new WebSocket(wssBarsUrl);

        this.barsWs.onopen = () => {
            console.log('Binance opened');
        };

        this.barsWs.onclose = () => {
            console.log('Binance closed');
        };

        this.barsWs.onmessage = (msg) => {
            let data = JSON.parse(msg.data);
            if (data.e !== 'kline') {
                return;
            }

            let bar = {
                time: data.E,
                open: data.k.o,
                high: data.k.h,
                low: data.k.l,
                close: data.k.c,
                volume: data.k.v,
            };

            onRealtimeCallback(bar);
        };

        this.barsWs.onerror = function(error) {
            console.log("Binance error " + error.message);
        };
    },
}