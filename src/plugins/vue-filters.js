import Vue from 'vue';
import _ from 'lodash';

Vue.filter('join', function (value, precision) {
  if (!Array.isArray(value)) {
    return value;
  }
  return value.join(', ');
});

Vue.filter('round', function (value, precision = 2, fixed = false) {
  if (isNaN(value)) {
      return value;
  }
  value = +value;
  value = Math.round(value * Math.pow(10, precision)) / Math.pow(10, precision);
  return fixed ? value.toFixed(precision) : value
});

Vue.filter('prettyNumber', function (value) {
    if (value < 1000) {
      return (Math.round(value)).toFixed(2);
    } else if (value >= 1000 && value < 10000) {
      return (value/1000).toFixed(2).replace(/[0]+$/, '').replace(/\.$/, '') + 'K';
    } else if ((value >= 10000 && value <= 1000000)) {
        return (value/1000).toFixed(2) + 'K';
    } else if ((value >= 1000000 && value <= 10000000)) {
        return (value/1000000).toFixed(2).replace(/[0]+$/, '').replace(/\.$/, '') + 'M';
    } else {
        return (value/1000000).toFixed(2) + 'M';
    }
});

Vue.filter('formatDate', function (value, type = 'fullDate', timestamp = false) {
    let d = !timestamp ? new Date(value) : new Date(value * 1000),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hours = '' + d.getHours(),
        minutes = '' + d.getMinutes(),
        seconds = '' + d.getSeconds();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (hours.length < 2) hours = '0' + hours;
    if (minutes.length < 2) minutes = '0' + minutes;
    if (seconds.length < 2) seconds = '0' + seconds;

    switch (type) {
      case 'fullTime': return  [hours, minutes, seconds].join(':');
      case 'fullDate': return [day, month, year].join('.') + ' ' + [hours, minutes, seconds].join(':');
      case 'onlyDate': return [day, month, year].join('.');
    }


});

Vue.filter('trimZeroes', function (value) {
    if (value) {
        if (value.toString().indexOf('.') === -1) {
            return value;
        }
        return value.toString().replace(/[0]+$/, '').replace(/\.$/, '');
    }

    return '';
});

Vue.filter('spacifyNumber', function (value) {
  return new Intl.NumberFormat('ru-RU').format(value);
});

/**
* Строка с заглавной буквы
* @param value
* @return {string}
*/
Vue.filter('capitalize', function (value) {
  return _.capitalize(value);
});

/**
* Строка в верхний регистр
* @param value
* @return {string}
*/
Vue.filter('uppercase', function (value) {
  return _.toUpper(value);
});

/**
* Строка в нижний регистр
* @param value
* @return {string}
*/
Vue.filter('lowercase', function (value) {
  return _.toLower(value);
});

/**
* Функция предназначена для склонения слов
* используемых вместе с числительными.
* @param number
* @param words
* @return string
*/
Vue.filter('transChoose', function (number, words) {
  let cases = [2, 0, 1, 1, 1, 2];
  return words[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
});

/**
* Если переменная не задана вернуть значение по умолчанию
* @param value
* @param defaultValue
* @return {*}
*/
Vue.filter('default', function (value, defaultValue) {
  return !value ? defaultValue : value;
});

/**
* Перевод копеек/центов в рубли/доллары
* @param value
* @return string
*/
Vue.filter('scaleMoney', function (value) {
  let v = value.toString();
  if (value < 10) {
      return '0.0' + v;
  } else if (value < 100) {
      return '0.' + v;
  }

  return v.slice(0, -2) + (v.slice(-2) === '00' ? '' : '.' + v.slice(-2));
});
