import Vue from 'vue';
import TVChartContainer from '~/components/trading/TVChartContainer';

Vue.component('tv-chart-container', TVChartContainer)
