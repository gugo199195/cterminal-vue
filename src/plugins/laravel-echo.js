import Echo from 'laravel-echo'
window.io = require('socket.io-client');

export default () => {
    const cookies = document.cookie.split('; ').map(stringCookie => stringCookie.split('='));
    const cookie = cookies.find(cookie => cookie[0] === 'auth._token.local');

    let auth = null;
    if (cookie) {
        auth = {headers: {Authorization: cookie[1].replace('%20', ' ')}}
    }

    if (typeof io !== 'undefined') {
        window.Echo = new Echo({
            broadcaster: 'socket.io',
            host: 'https://echo.hicotrading.com',
            auth: auth,
        });
    }
}