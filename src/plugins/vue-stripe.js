import Vue from 'vue'
import { StripeCheckout } from 'vue-stripe'

Vue.component('stripe-checkout', StripeCheckout);
