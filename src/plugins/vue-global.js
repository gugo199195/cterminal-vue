process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
import Vue from 'vue';
import AsyncComputed from 'vue-async-computed';
import DomMoveDirective from '@/assets/js/directives/domMove.js';
import EraseControlDirective from '@/assets/js/directives/eraseControl.js';
import ShowPasswordControlDirective from '~/assets/js/directives/showPasswordControl.js';
import FloatFilterDirective from '../assets/js/directives/floatFilter.js';
import Vuetify from 'vuetify';
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'vuetify/dist/vuetify.min.css'
import VueCookie from 'vue-cookie';
import VueTour from 'vue-tour';
import 'vue-tour/dist/vue-tour.css';

Vue.use(AsyncComputed);
Vue.use(VueCookie);
Vue.use(VueTour)
Vue.use(Vuetify, {
    iconfont: 'mdi',
    theme: {
        primary: '#2cbd6e',
        secondary: '#63919e',
        accent: '#2cbd6d',
    },
    options: {
        customProperties: true
    },
    icons: {
        'checkboxOn': 'check_box',
        'checkboxOff': 'check_box_outline_blank',
        'radioOn': 'radio_button_checked',
        'radioOff': 'radio_button_unchecked',
    }
})
Vue.directive('dom-move', DomMoveDirective);
Vue.directive('erase-control', EraseControlDirective);
Vue.directive('show-password-control', ShowPasswordControlDirective);

Vue.directive("float-filter", FloatFilterDirective);