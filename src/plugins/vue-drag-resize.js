import Vue from 'vue'
import VueDragResize from './vue-drag-resize/src/index'

Vue.component('vue-drag-resize', VueDragResize)
