import Vue from 'vue';
import 'beautify-scrollbar/dist/index.css';
import lazyLoadList from 'vue-lazy-load-list';

Vue.use(lazyLoadList);