export default function ({ isHMR, app, store, route, req }) {

    if (isHMR) {
        return;
    }

    if (req) {
        if (route.name) {
            let darkTheme = false;

            if (req.headers.cookie) {
                const cookies = req.headers.cookie.split('; ').map(stringCookie => stringCookie.split('='));
                const cookie = cookies.find(cookie => cookie[0] === 'darkTheme');

                if (cookie) {
                    darkTheme = cookie[1] === 'true';
                }
            }
            store.commit('actionManager/setColorTheme', darkTheme);
        }
    }
};
